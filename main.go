package main

import (
	"fmt"
	"math"
	qsp "qspgo-faster/qsp/ncircuit"
	"strconv"
)

func main() {
	qr1, _ := qsp.CreateRegister(5)
	th2 := (math.Pi * 2) / math.Pow(2, 2)
	th3 := (math.Pi * 2) / math.Pow(2, 3)
	qr1.H(0)
	qr1.H(1)
	qr1.H(2)
	// qr1.CNOT(2, 3)
	// qr1.CNOT(2, 4)
	qr1.H(1)
	qr1.CU(th2, 1, 0)
	qr1.H(0)
	qr1.CU(th3, 1, 2)
	qr1.CU(th2, 0, 2)

	res := qr1.Measure()
	for i := 0; i < qr1.N; i++ {
		num := int(math.Log2(float64(len(res))))
		sti := strconv.FormatInt(int64(i), 2)
		its, _ := strconv.Atoi(sti)
		str := "%0" + strconv.Itoa(num) + "d"
		fmt.Println(fmt.Sprintf(str, its), ": ", res[i])
	}
	// for i := 0; i < qc.R[0].N; i++ {
	// 	fmt.Println(*qc.R[0].Q[i])
	// }
	// fmt.Println(res)
	// fmt.Println(len(res))
}

// qr1, _ := qsp.CreateRegister(5)
// th2 := (math.Pi * 2) / math.Pow(2, 2)
// th3 := (math.Pi * 2) / math.Pow(2, 3)
// qr1.H(0)
// qr1.H(1)
// qr1.H(2)
// qr1.CNOT(2, 3)
// qr1.CNOT(2, 4)
// qr1.H(1)
// qr1.CU(th2, 1, 0)
// qr1.H(0)
// qr1.CU(th3, 1, 2)
// qr1.CU(th2, 0, 2)

// qr1, _ := qsp.CreateRegister(3)
// qr2, _ := qsp.CreateRegister(1)
// qc, _ := qsp.CreateCircuit(qr1, qr2)
// qr1.X(0)
// qr1.X(2)
// qr1.QFT(3)
// qr1.QFTi(3)
// res := qc.R[0].Measure()
// N := len(res)

// qr1, _ := qsp.CreateRegister(3)
// qr2, _ := qsp.CreateRegister(2)
// qc, _ := qsp.CreateCircuit(qr1, qr2)
// th2 := (math.Pi * 2) / math.Pow(2, 2)
// th3 := (math.Pi * 2) / math.Pow(2, 3)
// qr1.H(0)
// qr1.H(1)
// qr1.H(2)
// qc.CNOT(0, 2, 1, 0)
// qc.CNOT(0, 2, 1, 1)
// qr1.H(1)
// qr1.CU(th2, 1, 0)
// qr1.H(0)
// qr1.CU(th3, 1, 2)
// qr1.CU(th2, 0, 2)
