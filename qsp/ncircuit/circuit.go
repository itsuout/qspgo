package qsp

import (
	"errors"
	"fmt"
	"strconv"
)

// QuRegister is a quantum register that contain n qubit(s)
type QuRegister struct {
	N int
	Q []*[]complex128
}

// QuCircuit is a standard quantum circuit but not is not provide Undo()
type QuCircuit struct {
	N int
	R []*QuRegister
}

// Result is a struct that contains state name and, p(StateValue[0]|0>) and p(StateValue[1]|1>) of |state>t
type Result struct {
	StateName  string
	StateValue string
}

// Results is a collection of Result
type Results []Result

// CreateRegister is a function to create quantum circuit with n qubits
func CreateRegister(nqb int) (*QuRegister, error) {
	if nqb == 0 || nqb >= 33 {
		return nil, errors.New("error: connot create a quantum circuit with " + strconv.Itoa(nqb) + "qubit(s)")
	}
	var (
		qb = make([]*[]complex128, 0, nqb)
	)

	for i := 0; i < nqb; i++ {
		qb = append(qb, &[]complex128{complex(1, 0), complex(0, 0)})
	}

	ret := QuRegister{
		N: nqb,
		Q: qb,
	}

	return &ret, nil
}

// CreateCircuit provides circuit with qrn register
func CreateCircuit(qrn ...*QuRegister) (*QuCircuit, error) {
	n := len(qrn)
	if n == 0 {
		return nil, errors.New("range Error: cannot create circuit with 0 register")
	}
	qr := make([]*QuRegister, 0, n)
	qr = append(qr, qrn...)

	return &QuCircuit{
		N: n,
		R: qr,
	}, nil
}

// Print return string like |qn> = c0|0> + c1|1>
// func (qc *QuCircuit) Print(n int) string {
// 	var (
// 		buf bytes.Buffer
// 		re0 string
// 		re1 string
// 		im0 string
// 		im1 string
// 	)

// 	buf.WriteString("|q")
// 	buf.WriteString(strconv.Itoa(n))
// 	buf.WriteString("> = ")

// 	re0 = fmt.Sprintf("%0.3f", real(qc.Qubit[n][0]))
// 	im0 = fmt.Sprintf("%0.3f", imag(qc.Qubit[n][0]))
// 	re1 = fmt.Sprintf("%0.3f", real(qc.Qubit[n][1]))
// 	im1 = fmt.Sprintf("%0.3f", imag(qc.Qubit[n][1]))

// 	buf.WriteByte('(')
// 	buf.WriteString(re0)
// 	buf.WriteString(", ")
// 	buf.WriteString(im0)
// 	buf.WriteString(")|0> + (")
// 	buf.WriteString(re1)
// 	buf.WriteString(", ")
// 	buf.WriteString(im1)
// 	buf.WriteString(")|1>")

// 	ret := buf.String()
// 	return ret
// }

// PrintCurcuit return all states of qubits in circuit as |qn> = c0|0> + c1|1>
// func (qc *QuCircuit) PrintCurcuit() []string {
// 	var (
// 		n   = len(qc.Qubit)
// 		ret = make([]string, 0, n)
// 	)

// 	for i := 0; i < n; i++ {
// 		ret = append(ret, qc.Print(i))
// 	}

// 	return ret
// }

// Results tha will be used with api
// func (qc *QuCircuit) Results() Results {
// 	var (
// 		state  = qc.Measure(100)
// 		n      = len(state)
// 		ret    = make(Results, 0, n)
// 		pad    = fmt.Sprintf("%s%ds", "%0", int(math.Log2(float64(n))))
// 		result Result
// 	)

// 	for i := 0; i < n; i++ {
// 		result.StateName = fmt.Sprintf(pad, strconv.FormatInt(int64(i), 2))
// 		result.StateValue = fmt.Sprintf("%0.3d", state[i])

// 		ret = append(ret, result)
// 	}
// 	return ret
// }

// Hello from QSP
func Hello() {
	fmt.Println("Hello from QSPGo")
}
