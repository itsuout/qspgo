package qsp

import (
	"math"
	"math/cmplx"

	"gonum.org/v1/gonum/blas"
	cb "gonum.org/v1/gonum/blas/cblas128"
)

// QuGate is a matrix that must be either hemitian or unitary matrix
type QuGate struct {
	N int
	M cb.General
}

type crossRegister interface {
	ApplyN()
	Swap()
	CNOT()
	CS()
	CT()
	Toffoli()
	Fredkin()
	CROT()
	UROT()
}

// GetXOR return Classical XOR gate
func GetXOR() *QuGate {
	return &QuGate{
		N: 1,
		M: cb.General{
			Rows:   2,
			Cols:   2,
			Stride: 2,
			Data: []complex128{
				1, 0,
				0, 1,
			},
		},
	}
}

// GetNOT return Classical NOT gate
func GetNOT() *QuGate {
	return &QuGate{
		N: 1,
		M: cb.General{
			Rows:   2,
			Cols:   2,
			Stride: 2,
			Data: []complex128{
				0, 1,
				1, 0,
			},
		},
	}
}

// Quantum Gates

// GetX return Pauli X gate
func GetX() *QuGate {
	return &QuGate{
		N: 1,
		M: cb.General{
			Rows:   2,
			Cols:   2,
			Stride: 2,
			Data: []complex128{
				0, 1,
				1, 0,
			},
		},
	}
}

// GetY return Pauli Y gate
func GetY() *QuGate {
	return &QuGate{
		N: 1,
		M: cb.General{
			Rows:   2,
			Cols:   2,
			Stride: 2,
			Data: []complex128{
				0, -1i,
				1i, 0,
			},
		},
	}
}

// GetZ return Pauli Z gate
func GetZ() *QuGate {
	return &QuGate{
		N: 1,
		M: cb.General{
			Rows:   2,
			Cols:   2,
			Stride: 2,
			Data: []complex128{
				1, 0,
				0, -1,
			},
		},
	}
}

// GetI return Identity gate
func GetI() *QuGate {
	return &QuGate{
		N: 1,
		M: cb.General{
			Rows:   2,
			Cols:   2,
			Stride: 2,
			Data: []complex128{
				1, 0,
				0, 1,
			},
		},
	}
}

// GetH return Hadamard gate
func GetH() *QuGate {
	osq := complex(1/math.Sqrt2, 0)
	return &QuGate{
		N: 1,
		M: cb.General{
			Rows:   2,
			Cols:   2,
			Stride: 2,
			Data: []complex128{
				osq, osq,
				osq, complex(-1/math.Sqrt2, 0),
			},
		},
	}
}

// GetS return S gate
func GetS() *QuGate {
	return &QuGate{
		N: 1,
		M: cb.General{
			Rows:   2,
			Cols:   2,
			Stride: 2,
			Data: []complex128{
				1, 0,
				0, 1i,
			},
		},
	}
}

// GetSDagger return conjugated S gate
func GetSDagger() *QuGate {
	return &QuGate{
		N: 1,
		M: cb.General{
			Rows:   2,
			Cols:   2,
			Stride: 2,
			Data: []complex128{
				1, 0,
				0, -1i,
			},
		},
	}
}

// GetT return T gate
func GetT() *QuGate {
	PI4 := math.Pi / 4
	return &QuGate{
		N: 1,
		M: cb.General{
			Rows:   2,
			Cols:   2,
			Stride: 2,
			Data: []complex128{
				complex(1, 0), complex(0, 0),
				complex(0, 0), complex(math.Cos(PI4), math.Sin(PI4)),
			},
		},
	}
}

// GetTDagger return conjugated T gate
func GetTDagger() *QuGate {
	PI4 := math.Pi / 4
	return &QuGate{
		N: 1,
		M: cb.General{
			Rows:   2,
			Cols:   2,
			Stride: 2,
			Data: []complex128{
				complex(1, 0), complex(0, 0),
				complex(0, 0), complex(math.Cos(PI4), math.Sin(PI4)),
			},
		},
	}
}

// GetRx return Rx(theta) gate
func GetRx(theta float64) *QuGate {
	return &QuGate{
		N: 1,
		M: cb.General{
			Rows:   2,
			Cols:   2,
			Stride: 2,
			Data: []complex128{
				complex(math.Cos(theta/2), 0), complex(0, -math.Sin(theta/2)),
				complex(0, -math.Sin(theta/2)), complex(math.Cos(theta/2), 0),
			},
		},
	}
}

// GetRy return Ry(theta) gate
func GetRy(theta float64) *QuGate {
	return &QuGate{
		N: 1,
		M: cb.General{
			Rows:   2,
			Cols:   2,
			Stride: 2,
			Data: []complex128{
				complex(math.Cos(theta/2), 0), -complex(math.Sin(theta/2), 0),
				complex(math.Sin(theta/2), 0), complex(math.Cos(theta/2), 0),
			},
		},
	}
}

// GetRz return Rz(theta) gate
func GetRz(rho float64) *QuGate {
	return &QuGate{
		N: 1,
		M: cb.General{
			Rows:   2,
			Cols:   2,
			Stride: 2,
			Data: []complex128{
				complex(math.Cos(rho/2), -math.Sin(rho/2)), 0,
				0, complex(math.Cos(rho/2), math.Sin(rho/2)),
			},
		},
	}
}

//GetU return U gate
func GetU(theta float64) *QuGate {
	return &QuGate{
		N: 1,
		M: cb.General{
			Rows:   2,
			Cols:   2,
			Stride: 2,
			Data: []complex128{
				1, 0,
				0, cmplx.Exp(complex(0, theta)),
			},
		},
	}
}

//GetU return U gate
// func GetU(theta float64) *QuGate {
// 	return &QuGate{
// 		N: 1,
// 		M: cb.General{
// 			Rows:   2,
// 			Cols:   2,
// 			Stride: 2,
// 			Data: []complex128{
// 				1, 0,
// 				0, complex(math.Cos(theta), math.Sin(theta)),
// 			},
// 		},
// 	}
// }

// The following are multiples qubit gates

// GetCU return controlled-U gate
// func GetCU(theta float64) *QuGate {
// 	return &QuGate{
// 		N: 2,
// 		M: cb.General{
// 			Rows:   4,
// 			Cols:   4,
// 			Stride: 4,
// 			Data: []complex128{
// 				1, 0, 0, 0,
// 				0, 1, 0, 0,
// 				0, 0, 1, 0,
// 				0, 0, 0, complex(math.Cos(theta), math.Sin(theta)),
// 			},
// 		},
// 	}
// }

// GetCU return controlled-U gate
func GetCU(theta float64) *QuGate {
	return &QuGate{
		N: 2,
		M: cb.General{
			Rows:   4,
			Cols:   4,
			Stride: 4,
			Data: []complex128{
				1, 0, 0, 0,
				0, 1, 0, 0,
				0, 0, 1, 0,
				0, 0, 0, cmplx.Exp(complex(0, theta)),
			},
		},
	}
}

// GetCNOT return Controlled-NOT gate
func GetCNOT() *QuGate {
	return &QuGate{
		N: 2,
		M: cb.General{
			Rows:   4,
			Cols:   4,
			Stride: 4,
			Data: []complex128{
				1, 0, 0, 0,
				0, 1, 0, 0,
				0, 0, 0, 1,
				0, 0, 1, 0,
			},
		},
	}
}

// GetSwap return Swap gate
func GetSwap() *QuGate {
	return &QuGate{
		N: 2,
		M: cb.General{
			Rows:   4,
			Cols:   4,
			Stride: 4,
			Data: []complex128{
				1, 0, 0, 0,
				0, 0, 1, 0,
				0, 1, 0, 0,
				0, 0, 0, 1,
			},
		},
	}
}

// GetCZ return Controlled-Z gate
func GetCZ() *QuGate {
	return &QuGate{
		N: 2,
		M: cb.General{
			Rows:   4,
			Cols:   4,
			Stride: 4,
			Data: []complex128{
				1, 0, 0, 0,
				0, 1, 0, 0,
				0, 0, 1, 0,
				0, 0, 0, -1,
			},
		},
	}
}

// GetCS return Controlled-S gate
func GetCS() *QuGate {
	return &QuGate{
		N: 2,
		M: cb.General{
			Rows:   4,
			Cols:   4,
			Stride: 4,
			Data: []complex128{
				1, 0, 0, 0,
				0, 1, 0, 0,
				0, 0, 1, 0,
				0, 0, 0, -1i,
			},
		},
	}
}

// GetCT return Controlled-S gate
func GetCT() *QuGate {
	PI4 := math.Pi / 4
	return &QuGate{
		N: 2,
		M: cb.General{
			Rows:   4,
			Cols:   4,
			Stride: 4,
			Data: []complex128{
				1, 0, 0, 0,
				0, 1, 0, 0,
				0, 0, 1, 0,
				0, 0, 0, complex(math.Cos(PI4), math.Sin(PI4)),
			},
		},
	}
}

// GetCR return Controlled-S gate
func GetCR(k int) *QuGate {
	theta := (math.Pi * 2) / math.Pow(2, float64(k))
	return &QuGate{
		N: 2,
		M: cb.General{
			Rows:   4,
			Cols:   4,
			Stride: 4,
			Data: []complex128{
				1, 0, 0, 0,
				0, 1, 0, 0,
				0, 0, 1, 0,
				0, 0, 0, complex(math.Cos(theta), math.Sin(theta)),
			},
		},
	}
}

// GetToffoli return Toffoli gate
func GetToffoli() *QuGate {
	return &QuGate{
		N: 3,
		M: cb.General{
			Rows:   8,
			Cols:   8,
			Stride: 8,
			Data: []complex128{
				1, 0, 0, 0, 0, 0, 0, 0,
				0, 1, 0, 0, 0, 0, 0, 0,
				0, 0, 1, 0, 0, 0, 0, 0,
				0, 0, 0, 1, 0, 0, 0, 0,
				0, 0, 0, 0, 1, 0, 0, 0,
				0, 0, 0, 0, 0, 1, 0, 0,
				0, 0, 0, 0, 0, 0, 0, 1,
				0, 0, 0, 0, 0, 0, 1, 0,
			},
		},
	}
}

// GetFredkin return Fredkin gate
func GetFredkin() *QuGate {
	return &QuGate{
		N: 3,
		M: cb.General{
			Rows:   8,
			Cols:   8,
			Stride: 8,
			Data: []complex128{
				1, 0, 0, 0, 0, 0, 0, 0,
				0, 1, 0, 0, 0, 0, 0, 0,
				0, 0, 1, 0, 0, 0, 0, 0,
				0, 0, 0, 1, 0, 0, 0, 0,
				0, 0, 0, 0, 1, 0, 0, 0,
				0, 0, 0, 0, 0, 0, 1, 0,
				0, 0, 0, 0, 0, 1, 0, 0,
				0, 0, 0, 0, 0, 0, 0, 1,
			},
		},
	}
}

// The following are single qubit gate operations

// X is a function that will do Pauli X on qubit n
func (qr *QuRegister) X(n int) {
	x := GetX()
	qr.ApplyOne(x, n)
}

// Y is a function that will do Pauli Y on a qubit n
func (qr *QuRegister) Y(n int) {
	y := GetY()
	qr.ApplyOne(y, n)
}

// Z is a function that will do Pauli Z on qubit n
func (qr *QuRegister) Z(n int) {
	z := GetZ()
	qr.ApplyOne(z, n)
}

// I is a function that will do I on qubit n
func (qr *QuRegister) I(n int) {
	i := GetI()
	qr.ApplyOne(i, n)
}

// H is a function that will do Hadamard on qubit n
func (qr *QuRegister) H(n int) {
	h := GetH()
	qr.ApplyOne(h, n)
}

// S is a function that will do S on qubit n
func (qr *QuRegister) S(n int) {
	s := GetS()
	qr.ApplyOne(s, n)
}

// SDagger is a function that will do SDagger on qubit n
func (qr *QuRegister) SDagger(n int) {
	sd := GetSDagger()
	qr.ApplyOne(sd, n)
}

// T is a function that will do T on qubit n
func (qr *QuRegister) T(n int) {
	t := GetT()
	qr.ApplyOne(t, n)
}

// TDagger is a function that will do TDagger on qubit n
func (qr *QuRegister) TDagger(n int) {
	td := GetTDagger()
	qr.ApplyOne(td, n)
}

// Ry is a function that will do Ry(theta) gate on qubit n
func (qr *QuRegister) Ry(theta float64, n int) {
	ry := GetRy(theta)
	qr.ApplyOne(ry, n)
}

// Rx is a function that will do Rx(theta) on qubit n
func (qr *QuRegister) Rx(theta float64, n int) {
	rx := GetRx(theta)
	qr.ApplyOne(rx, n)
}

// U is a function that will do U1 on qubit n
func (qr *QuRegister) U(theta float64, n int) {
	u := GetU(theta)
	qr.ApplyOne(u, n)
}

// The following are multiple qubit gate operation

// Swap is a function that will swap value of qubit m, n
func (qr *QuRegister) Swap(m, n int) {
	qr.Q[n], qr.Q[m] = qr.Q[m], qr.Q[n]
}

// CU is a function that will do CU on qubit n controlled by qubit m
func (qr *QuRegister) CU(theta float64, m, n int) {
	qr.ApplyN(GetCU(theta), m, n)
}

// CNOT is a function that will do CNOT on qubit n if value of qubit m is == 1
func (qr *QuRegister) CNOT(m, n int) {
	qr.ApplyN(GetCNOT(), m, n)
}

// CZ is a function that will do Z on qubit n if value of qubit m is == 1
func (qr *QuRegister) CZ(m, n int) {
	qr.ApplyN(GetCZ(), m, n)
}

// CS is a function that will do S on qubit n if value of qubit m is == 1
func (qr *QuRegister) CS(m, n int) {
	qr.ApplyN(GetCS(), m, n)
}

// CT is a function that will do S on qubit n if value of qubit m is == 1
func (qr *QuRegister) CT(m, n int) {
	qr.ApplyN(GetCT(), m, n)
}

// CR is a function that will do Control-UROT with m control on n qubit
func (qr *QuRegister) CR(k, m, n int) {
	qr.ApplyN(GetCR(k), m, n)
}

// Toffoli is a function that will do a NOT gate on o if value of m and n == 1
func (qr *QuRegister) Toffoli(m, n, o int) {
	qr.ApplyN(GetToffoli(), m, n, o)
}

// Fredkin is a function that will swap values of qubit n and o ig  value of qubit m == 1
func (qr *QuRegister) Fredkin(m, n, o int) {
	qr.ApplyN(GetFredkin(), m, n, o)
}

// Swap is a function that will swap value of qubit m, n
func (qc *QuCircuit) Swap(m, n, o, p int) {
	*qc.R[m].Q[n], *qc.R[o].Q[p] = *qc.R[o].Q[p], *qc.R[m].Q[n]
}

// CNOT is a function that will do CNOT on qubit n if value of qubit m is == 1
func (qc *QuCircuit) CNOT(qi ...int) {
	qc.ApplyN(GetCNOT(), qi...)
}

// CZ is a function that will do Z on qubit n if value of qubit m is == 1
func (qc *QuCircuit) CZ(qi ...int) {
	qc.ApplyN(GetCZ(), qi...)
}

// CS is a function that will do CS on qubit n if value of qubit m is == 1
func (qc *QuCircuit) CS(qi ...int) {
	qc.ApplyN(GetCS(), qi...)
}

// CU is a function that will do CU on qubit n controlled by qubit m
func (qc *QuCircuit) CU(theta float64, qi ...int) {
	qc.ApplyN(GetCU(theta), qi...)
}

// Toffoli is a function that will do a NOT gate on o if value of m and n == 1
func (qc *QuCircuit) Toffoli(qi ...int) {
	qc.ApplyN(GetToffoli(), qi...)
}

// Fredkin is a function that will swap values of qubit n and o ig  value of qubit m == 1
func (qc *QuCircuit) Fredkin(qi ...int) {
	qc.ApplyN(GetFredkin(), qi...)
}

// Dagger is a operation that return complex conjugate transpost of a matrix
// in this case is our gate
func (g *QuGate) Dagger() {
	var (
		a = cb.General{
			Rows:   g.M.Rows,
			Cols:   g.M.Cols,
			Stride: g.M.Rows,
			Data:   []complex128{},
		}
		ret = cb.General{
			Rows:   a.Rows,
			Cols:   a.Cols,
			Stride: a.Cols,
			Data:   make([]complex128, a.Rows*a.Cols),
		}
	)
	for i := 0; i < a.Rows*a.Cols; i++ {
		a.Data = append(a.Data, 1)
	}
	cb.Gemm(blas.NoTrans, blas.ConjTrans, 1, a, g.M, 0, ret)
	g.M = ret
}
