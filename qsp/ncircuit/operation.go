package qsp

import (
	crypto_rand "crypto/rand"
	"encoding/binary"
	"fmt"
	"math"
	"math/cmplx"
	math_rand "math/rand"
	"os"
	"sync"

	"gonum.org/v1/gonum/blas"
	cb "gonum.org/v1/gonum/blas/cblas128"
)

func isRangeMismatch(N int, qi ...int) error {
	for i := 0; i < len(qi); i++ {
		if qi[i] < 0 || qi[i] >= N {
			fmt.Println("error: range mismatch, there is no qubit", qi[i], "in a register")
			os.Exit(1)
		}
	}
	return nil
}

func isDimensionMismatch(N int, n int) error {
	if N != n {
		fmt.Println("error: dimension mismatch, gate is a", N, "qubit(s) gate but is apply to", n, "qubit(s)")
		os.Exit(1)
	}
	return nil
}

// gemv return product of a matrix and a ket
func gemv(gateData cb.General, state []complex128) []complex128 {
	var (
		p = len(state)
		x = cb.Vector{
			N:    p,
			Inc:  1,
			Data: state,
		}
		ret = cb.Vector{
			N:    p,
			Inc:  1,
			Data: make([]complex128, p),
		}
	)
	cb.Gemv(blas.NoTrans, 1, gateData, x, 1, ret)
	return ret.Data
}

// cbn return |left> kronecker |right>
func cbn(left, right []complex128) []complex128 {
	var (
		n      = len(left)
		p      = len(right)
		s      = n * p
		ret    = make([]complex128, s)
		kn, km int
	)
	for i := 0; i < s; i++ {
		kn = int(i / 2)
		km = i % 2
		ret[i] += left[kn] * right[km]
	}
	return ret
}

// combine return |left> kronecker |right>
func combine(qb [][]complex128) []complex128 {
	N := len(qb)
	state := cbn(qb[0], qb[1])

	for i := 2; i < N; i++ {
		state = cbn(state, qb[i])
	}
	return state
}

// return a state vector
func (qr *QuRegister) state(qi ...int) []complex128 {
	N := len(qi)
	switch N {
	case 1:
		return *qr.Q[qi[0]]
	case 2:
		return cbn(*qr.Q[qi[0]], *qr.Q[qi[1]])
	default:
		qb := make([][]complex128, 0)
		if N == 0 {
			for i := 0; i < qr.N; i++ {
				qb = append(qb, *qr.Q[i])
			}
		} else {
			for i := 0; i < N; i++ {
				qb = append(qb, *qr.Q[qi[i]])
			}
		}
		NQB := len(qb)
		switch NQB % 2 {
		case 0:
			return combine(qb)
		case 1:
			if NQB == 1 {
				return *qr.Q[0]
			}
			lastQ := qb[NQB-1]
			qb[NQB-1] = nil
			qb = qb[:NQB-1]
			state := combine(qb)
			return cbn(state, lastQ)
		}
	}
	return nil
}

// return a state vector
func (qc *QuCircuit) state(qb [][]complex128) []complex128 {
	N := len(qb)
	switch N {
	case 1:
		return qb[0]
	case 2:
		return cbn(qb[0], qb[1])
	default:
		if N == 0 {
			return nil
		}
		NQB := len(qb)
		switch NQB % 2 {
		case 0:
			return combine(qb)
		case 1:
			lastQ := qb[NQB-1]
			qb[NQB-1] = nil
			qb = qb[:NQB-1]
			state := combine(qb)
			return cbn(state, lastQ)
		}
	}
	return nil
}

// split is a function that will split a state into collection with log2(len(state)) qubits (inverseKronecker)
func split(state []complex128) [][]complex128 {
	var (
		s      = len(state)
		q      = int(math.Log2(float64(s)))
		ret    = make([][]complex128, q)
		kn, km int
	)
	for i := 0; i < q; i++ {
		ret[i] = []complex128{
			complex(0, 0),
			complex(0, 0),
		}
	}
	for i := 0; i < s; i++ {
		for j := 0; j < q; j++ {
			kn = q - 1 - j
			km = int(float64(i)/math.Pow(2, float64(kn))) % 2
			ret[j][km] += state[i]

		}
	}
	return ret
}

func applyAmp(amp [][]complex128) [][]complex128 {
	bZero := cb.Vector{
		N:    2,
		Inc:  1,
		Data: []complex128{1, 0},
	}
	bOne := cb.Vector{
		N:    2,
		Inc:  1,
		Data: []complex128{0, 1},
	}
	N := len(amp)
	ret := make([][]complex128, 0, N)
	tmp := make([]complex128, 0, 2)
	for i := 0; i < N; i++ {
		if amp[i][0] == 0 && amp[i][1] == 0 {
			switch i {
			case 0:
				amp[i][0] = 1
			case 1:
				amp[i][1] = 1
			}
		}
		cb.Scal(amp[i][0], bZero)
		cb.Scal(amp[i][1], bOne)
		cb.Axpy(1, bZero, bOne)
		tmp = append(tmp, bOne.Data...)
		ret = append(ret, tmp)
		tmp = nil
		bZero.Data[0], bZero.Data[1] = 1, 0
		bOne.Data[0], bOne.Data[1] = 0, 1
	}
	return ret
}

// ApplyOne will apply single qubit gate to nth qubit in register
func (qr *QuRegister) ApplyOne(gate *QuGate, n int) {
	isRangeMismatch(qr.N, n)
	isDimensionMismatch(gate.N, 1)
	// bZero := cb.Vector{
	// 	N:    2,
	// 	Inc:  1,
	// 	Data: []complex128{1, 0},
	// }
	// bOne := cb.Vector{
	// 	N:    2,
	// 	Inc:  1,
	// 	Data: []complex128{0, 1},
	// }
	// apg := gemv(gate.M, *qr.Q[n])
	// cb.Scal(apg[0], bZero)
	// cb.Scal(apg[1], bOne)
	// cb.Axpy(1, bZero, bOne)
	// *qr.Q[n] = bOne.Data
	*qr.Q[n] = gemv(gate.M, *qr.Q[n])
}

// ApplyN will apply multiple a qubit gate to len(qi) qubit in register
func (qr *QuRegister) ApplyN(gate *QuGate, qi ...int) error {
	N := len(qi)
	isRangeMismatch(qr.N, qi...)
	isDimensionMismatch(gate.N, N)

	qb := make([][]complex128, 0, N)
	for i := 0; i < N; i++ {
		qb = append(qb, *qr.Q[i])
	}

	state := qr.state(qi...)
	res := applyAmp(split(gemv(gate.M, state)))
	for i := 0; i < len(res); i++ {
		*qr.Q[qi[i]] = res[i]
	}
	return nil
}

// ApplyN will apply multiple a qubit gate to len(qi) qubit in register
// testing
// func (qr *QuRegister) ApplyN(gate *QuGate, qi ...int) error {
// 	N := len(qi)
// 	isRangeMismatch(qr.N, qi...)
// 	isDimensionMismatch(gate.N, N)

// 	qb := make([][]complex128, 0, N)
// 	for i := 0; i < N; i++ {
// 		qb = append(qb, *qr.Q[i])
// 	}

// 	state := qr.state(qi...)
// 	res := applyAmp(splitQ(gemv(gate.M, state), qb))
// 	for i := 0; i < len(res); i++ {
// 		*qr.Q[qi[i]] = res[i]
// 	}
// 	return nil
// }

// ApplyN will apply multiple a qubit gate to len(qi) qubit in register
func (qc *QuCircuit) ApplyN(gate *QuGate, qi ...int) error {
	N := len(qi)
	// isRangeMismatch(qr.N, qi...)
	isDimensionMismatch(gate.N, int(N/2))
	qubit := make([][]complex128, 0, N)
	for i := 0; i < N; i += 2 {
		qubit = append(qubit, *qc.R[qi[i]].Q[qi[i+1]])
	}

	state := qc.state(qubit)
	res := applyAmp(split(gemv(gate.M, state)))
	for i := 0; i < N; i += 2 {
		*qc.R[qi[i]].Q[qi[i+1]] = res[int(i/2)]
	}
	return nil
}

// ApplyN will apply multiple a qubit gate to len(qi) qubit in register
// testing
// func (qc *QuCircuit) ApplyN(gate *QuGate, qi ...int) error {
// 	N := len(qi)
// 	// isRangeMismatch(qr.N, qi...)
// 	isDimensionMismatch(gate.N, int(N/2))
// 	qubit := make([][]complex128, 0, N)
// 	for i := 0; i < N; i += 2 {
// 		qubit = append(qubit, *qc.R[qi[i]].Q[qi[i+1]])
// 	}

// 	state := qc.state(qubit)
// 	res := applyAmp(splitQ(gemv(gate.M, state), qubit))
// 	for i := 0; i < N; i += 2 {
// 		*qc.R[qi[i]].Q[qi[i+1]] = res[int(i/2)]
// 	}
// 	return nil
// }

// Measure return measurement of a circuit
func (qr *QuRegister) Measure(qi ...int) []string {
	// isRangeMismatch(qr.N, qi...)
	ext := qr.peek(qi...)
	ret := make([]string, 0, len(ext))
	for _, p := range ext {
		ret = append(ret, fmt.Sprintf("%.4f", p))
	}

	return ret
}

// norm return length of a (state) vector
// is this going to work
func norm(vec []complex128) complex128 {
	total := complex(0, 0)
	for i := 0; i < len(vec); i++ {
		total += cmplx.Pow(vec[i], 2)
	}

	return cmplx.Sqrt(total)
}

// norm return length of a (state) vector
// testing
// func norm(vec []complex128) float64 {
// 	n := len(vec)
// 	cV := cb.Vector{
// 		N:    n,
// 		Inc:  1,
// 		Data: vec,
// 	}
// 	nrm := cb.Nrm2(cV)
// 	return nrm
// }

// normalize a (state) vector
func normalize(vec *[]complex128) {
	n := len(*vec)
	norm := norm(*vec)
	for i := 0; i < n; i++ {
		(*vec)[i] /= norm
	}
}

// normalize a (state) vector
// testing
// func normalize(vec *[]complex128) {
// 	N := len(*vec)
// 	cV := cb.Vector{
// 		N:    N,
// 		Inc:  1,
// 		Data: *vec,
// 	}
// 	norm := cb.Nrm2(cV)
// 	fmt.Println(norm)
// 	cb.Scal(complex(1/norm, 0), cV)
// 	*vec = cV.Data
// }

// Peek return probability of a state vector
func (qr *QuRegister) peek(qi ...int) []float64 {
	var (
		ret   = make([]float64, 0)
		state = qr.state(qi...)
	)
	normalize(&state)
	for i := 0; i < len(state); i++ {
		ret = append(ret, real(cmplx.Pow(state[i], 2)))
	}
	return ret
}

// changeSeed random seed
// wg *sync.WaitGroup
func changeSeed(wg *sync.WaitGroup) {
	var b [8]byte
	_, err := crypto_rand.Read(b[:])
	if err != nil {
		panic("cannot seed math/rand package with cryptographically secure random number generator")
	}
	math_rand.Seed(int64(binary.LittleEndian.Uint64(b[:])))

	defer wg.Done()
}
