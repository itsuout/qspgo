package qsp

import (
	"math"
)

// QFT is an implementation of Quantum Fourier Transform
func (qr *QuRegister) QFT(n int) {
	var k int
	N := n - 1
	for i := 0; i < n; i++ {
		qr.H(i)
		for j := N; j > i; j-- {
			k = j - i + 1
			theta := (math.Pi * 2) / math.Pow(2, float64(k))
			qr.CU(theta, j, i)
		}
	}
	for i := 0; i < n/2; i++ {
		qr.Swap(i, (n-1)-i)
	}
}

// QFTi is an implementation of Quantum Fourier Transform
func (qr *QuRegister) QFTi(n int) {
	for i := 0; i < n/2; i++ {
		qr.Swap(i, (n-1)-i)
	}
	N := n - 1
	for i := N; i >= 0; i-- {
		k := 2
		for j := i + 1; j < n; j++ {
			// fmt.Println("k:", k, "controller:", j, "controlled:", i)
			theta := (math.Pi * -2) / math.Pow(2, float64(k))
			qr.CU(theta, j, i)
			k++
		}
		// fmt.Println("H:", i)
		qr.H(i)
	}
}

// QPE performs Quantum Phase Estimate with a theta on n qubits
// Wiki Version
func (qc *QuCircuit) QPE(a float64, m, o, p int) {
	N := qc.R[m].N
	qc.R[o].X(p)
	for i := 0; i < N; i++ {
		qc.R[m].H(i)
	}
	kn := 1
	for i := N - 1; i >= 0; i-- {
		for j := 0; j < kn; j++ {
			qc.CU(a, m, i, o, p)
		}
		kn *= 2
	}
	qc.R[m].QFTi(N)
}
