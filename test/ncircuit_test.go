package test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	qsp "gitlab.com/dashnkn/qspgo/qsp/ncircuit"
)

// import (
// 	"testing"

// 	"github.com/stretchr/testify/assert"
// 	"gitlab.com/dashnkn/qspgo/qsp"
// 	"gonum.org/v1/gonum/blas/cblas128"
// )

func TestApplyOneOnRegister(t *testing.T) {
	testcase := []complex128{
		0, 1,
	}

	qr, _ := qsp.CreateRegister(1)
	qr.X(0)
	output := (*qr.Q[0])

	assert.Equal(t, testcase, output, "they should be equal")
}

func TestApplyNonRegister(t *testing.T) {
	testcase := [][]complex128{
		{0, 1},
		{0, 1},
	}
	qr, _ := qsp.CreateRegister(2)
	qr.X(0)
	qr.CNOT(0, 1)
	output := [][]complex128{*qr.Q[0], *qr.Q[1]}
	assert.Equal(t, testcase, output, "they should be equal")
}

func TestApplyOneOnCircuit(t *testing.T) {
	testcase := []complex128{
		0, 1,
	}
	qr, _ := qsp.CreateRegister(1)
	qc, _ := qsp.CreateCircuit(qr)

	qc.R[0].X(0)
	output := *qc.R[0].Q[0]
	assert.Equal(t, testcase, output, "they should be equal")
}

func TestApplyNonCircuit(t *testing.T) {
	testcase := [][]complex128{
		{0, 1},
		{0, 1},
	}
	qr, _ := qsp.CreateRegister(2)
	qc, _ := qsp.CreateCircuit(qr)

	qc.R[0].X(0)
	qc.CNOT(0, 0, 0, 1)

	output := [][]complex128{*qc.R[0].Q[0], *qc.R[0].Q[1]}

	// output :=
	assert.Equal(t, testcase, output, "they should be equal")
}

func TestCrossRegister(t *testing.T) {
	testcase := [][]complex128{
		{1, 0}, // From Register 0
		{0, 1}, // From Register 1
	}

	qr1, _ := qsp.CreateRegister(1)
	qr2, _ := qsp.CreateRegister(1)
	qc, _ := qsp.CreateCircuit(qr1, qr2)

	qc.R[0].X(0)
	qc.Swap(0, 0, 1, 0)

	output := [][]complex128{*qc.R[0].Q[0], *qc.R[1].Q[0]}
	assert.Equal(t, testcase, output, "they should be equal")
}
