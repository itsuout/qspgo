# QSPGo-nigthly version

## Prerequitsite

### Go
this package compatible with go1.15.5, get it [here](https://golang.org/dl/)

### Packages
the math parts in QSPGo is base on gonum, please install these packages before using.

| name | installation |
| ------ | ------ |
| blas | go get -u "gonum.org/v1/gonum/blas" |
| cblas | go get -u "gonum.org/v1/gonum/blas/cblas128" |

and then get QSPGo

`go get -u "gitlab.com/dashnkn/qspgo/qsp"`

## using
right now QSPGo is still under construction, examples of usage are in test file.